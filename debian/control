Source: privacybadger
Section: web
Priority: optional
Uploaders: John Scott <jscott@posteo.net>
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@lists.alioth.debian.org>
Build-Depends:
 debhelper-compat (= 13),
Standards-Version: 4.5.1
Homepage: https://www.eff.org/privacybadger/
Rules-Requires-Root: no

Package: webext-privacy-badger
Architecture: all
Depends:
 fonts-open-sans,
 ${misc:Depends},
Enhances:
 chromium,
 firefox,
 firefox-esr,
Description: browser extension automatically learns to block invisible trackers
 Instead of keeping lists of what to block, Privacy Badger
 learns by watching which domains appear to be tracking you
 as you browse the Web.
 .
 Privacy Badger sends the Do Not Track signal with your browsing.
 If trackers ignore your wishes, your Badger will learn to block
 them. Privacy Badger starts blocking once it sees the same
 tracker on three different websites. Besides automatic tracker
 blocking, Privacy Badger removes outgoing link click tracking on
 Facebook, Google and Twitter, with more privacy protections on the
 way.
 .
 This package provides the extension for WebExtensions-compatible
 browsers.
